# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
class CustomClass:
    """
    클래스의 문서화 내용을 입력합니다.
    """


def print_hi(name):
    """
    함수의 문서화입니다.
    테스트 주석인데요~.
    :name: 이름입니다.
    :return: 없습니다!
    """
    destination_fl = 30 + 20
    print(destination_fl)
    # Use a breakpoint in the code line below to debug your script.
    print(f"Hi, {name}")  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == "__main__":
    print_hi("PyCharm")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
